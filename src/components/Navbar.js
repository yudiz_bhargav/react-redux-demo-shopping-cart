import React from 'react';

// connect is an HOC, i.e. It returns a component itself
import { connect } from 'react-redux';

const Navbar = ({ amount }) => {
  return (
    <nav>
      <div className='nav-center'>
        <h3>ReduxGear</h3>
        <div className='nav-container'>
          <svg xmlns='http://www.w3.org/2000/svg' viewBox='0 0 20 20'>
            <path d='M16 6v2h2l2 12H0L2 8h2V6a6 6 0 1 1 12 0zm-2 0a4 4 0 1 0-8 0v2h8V6zM4 10v2h2v-2H4zm10 0v2h2v-2h-2z' />
          </svg>
          <div className='amount-container'>
            <p className='total-amount'>{amount}</p>
          </div>
        </div>
      </div>
    </nav>
  );
};

// in this function, as a parameter, we have access to store
// basically giving our Navbar component access to the store just as you would with props
// this function lets us pick exactly which values we want to get access to from the state
const mapStateToProps = (state) => {
  // below code gives us access to an 'amount' prop in Navbar component
  // thus it gives us the property values we need to use in the current component from the state
  return { amount: state.amount };
};

// connect function when invoked, returns a functional component, and that returned component takes our current component (in this case it is the Navbar component) as an argument
// by using this syntax we get access to our state and dispatch method
export default connect(mapStateToProps)(Navbar);
