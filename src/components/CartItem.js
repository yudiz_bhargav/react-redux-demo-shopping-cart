import React from 'react';

import { connect } from 'react-redux';
import {
  DECREASE,
  INCREASE,
  REMOVE,
  TOGGLE_AMOUNT,
  removeItem,
} from '../actions';

const CartItem = (props) => {
  const { img, title, price, amount, remove, decrease, increase, toggle } =
    props;

  return (
    <article className='cart-item'>
      <img src={img} alt={title} />
      <div>
        <h4>{title}</h4>
        <h4 className='item-price'>${price}</h4>
        {/* remove button */}
        <button className='remove-btn' onClick={remove}>
          remove
        </button>
      </div>
      <div>
        {/* increase amount */}
        <button className='amount-btn' onClick={() => toggle('inc')}>
          <svg xmlns='http://www.w3.org/2000/svg' viewBox='0 0 20 20'>
            <path d='M10.707 7.05L10 6.343 4.343 12l1.414 1.414L10 9.172l4.243 4.242L15.657 12z' />
          </svg>
        </button>
        {/* amount */}
        <p className='amount'>{amount}</p>
        {/* decrease amount */}
        <button
          className='amount-btn'
          onClick={() => {
            if (amount === 1) {
              return remove();
            } else {
              return toggle('dec');
            }
          }}
        >
          <svg xmlns='http://www.w3.org/2000/svg' viewBox='0 0 20 20'>
            <path d='M9.293 12.95l.707.707L15.657 8l-1.414-1.414L10 10.828 5.757 6.586 4.343 8z' />
          </svg>
        </button>
      </div>
    </article>
  );
};

// in this code we don't need mapStateToProps because all the necessary props are already passed down to this component from CartContainer component

// with mapDispatchToProps you get the dispatch function as your prop for the current component (CartItem)
// when using mapDispatchToProps we also get a second parameter (ownProps) which are whatever props that are being passed into the component.. basically every single prop passed down to this component from the CartContainer component can be accessed with ownProps
const mapDispatchToProps = (dispatch, ownProps) => {
  const { id, amount } = ownProps;
  // we are not mapping our static state values to props in this case. In this case we want to set up our props as functions
  // use arrow function or else it will invoke dispatch even before any event occurs
  return {
    // payload helps you pass additional data to the reducer
    remove: () => dispatch(removeItem(id)),
    // remove: () => dispatch({ type: REMOVE, payload: { id } }),
    increase: () => dispatch({ type: INCREASE, payload: { id } }),
    decrease: () => dispatch({ type: DECREASE, payload: { id, amount } }),
    toggle: (toggle) =>
      dispatch({ type: TOGGLE_AMOUNT, payload: { id, toggle } }),
    // dispatch method - sends actions to store
    // actions (object) - MUST HAVE a type property - describing the kind of action to perform
    // DON'T MUTATE THE (old) STATE - redux built on immutability (copy)
  };
};

export default connect(null, mapDispatchToProps)(CartItem);
