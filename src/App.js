import React from 'react';

// components
import Navbar from './components/Navbar';
import CartContainer from './components/CartContainer';

// items data
// import cartItems from './cart-items';

// REDUX STUFF
import { createStore } from 'redux';
import reducer from './reducer';

// react redux --> Provider - wraps whole app ; connect function - used in components to get value from state
import { Provider } from 'react-redux';

// initial store (SECOND arg in createStore() func)
// initialStore is passed into redux function as state argument
// thus, initialStore sets initial state (state arg of reducer func)
// const initialStore = {
//   cart: cartItems,
//   total: 69,
//   amount: 5,
// };

// store stores data like state
const store = createStore(
  reducer /* initialStore */,
  window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__()
);

function App() {
  // cart setup

  return (
    // Provider component is looking for a store prop
    <Provider store={store}>
      <Navbar />
      <CartContainer />
    </Provider>
  );
}

export default App;
