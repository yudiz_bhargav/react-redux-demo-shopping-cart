// setting up actions like this prevents bugs and typos and helps you catch them quickly

export const DECREASE = 'DECREASE';
export const INCREASE = 'INCREASE';
export const REMOVE = 'REMOVE';
export const CLEAR_CART = 'CLEAR_CART';
export const GET_TOTALS = 'GET_TOTALS';
export const TOGGLE_AMOUNT = 'TOGGLE_AMOUNT';

// using action creator function to set dispatch({type: SOME_TYPE, payload: {something}})

// for now I will only change the REMOVE action type to a function

export const removeItem = (id) => {
  return { type: REMOVE, payload: { id } };
};
